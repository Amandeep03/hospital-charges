﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hospital_Charges
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml  
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void calcTotalCharges_Click(object sender, RoutedEventArgs e)
        {
            vm.TotalCharges();
        }

        private void calcMiscCharges_Click(object sender, RoutedEventArgs e)
        {
            vm.MiscCharges();
        }

        private void calcStayCharges_Click(object sender, RoutedEventArgs e)
        {
            vm.StayCharges();
        }
    }
}
