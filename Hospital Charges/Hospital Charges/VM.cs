﻿using System.ComponentModel;
using System.IO.Packaging;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Hospital_Charges
{
    class VM : INotifyPropertyChanged
    {
        private string numberOfDays;
        private string medicationCharges;
        private string surgicalCharges;
        private string labFees;
        private string physicalRehabitationCharges;
        private string errorForDaysInput;
        private string errorForMedicationCharges;
        private string errorForSurgicalCharges;
        private string errorForLabFees;
        private string errorForPhysicalRehabitationCharges;
        private string output;

        public string NumberOfDays { get => numberOfDays; set { numberOfDays = value; OnChange(); } }
        public string MedicationCharges { get => medicationCharges; set { medicationCharges = value; OnChange(); } }
        public string SurgicalCharges { get => surgicalCharges; set { surgicalCharges = value; OnChange(); } }
        public string LabFees { get => labFees; set { labFees = value; OnChange(); } }
        public string PhysicalRehabitationCharges { get => physicalRehabitationCharges; set { physicalRehabitationCharges = value; OnChange(); } }
        public string ErrorForDaysInput { get => errorForDaysInput; set { errorForDaysInput = value; OnChange(); } }
        public string ErrorForMedicationCharges { get => errorForMedicationCharges; set { errorForMedicationCharges = value; OnChange(); } }
        public string ErrorForSurgicalCharges { get => errorForSurgicalCharges; set { errorForSurgicalCharges = value; OnChange(); } }
        public string ErrorForLabFees { get => errorForLabFees; set { errorForLabFees = value; OnChange(); } }
        public string ErrorForPhysicalRehabitationCharges { get => errorForPhysicalRehabitationCharges; set { errorForPhysicalRehabitationCharges = value; OnChange(); } }
        public string Output { get => output; set { output = value; OnChange(); } }

        private decimal CalcStayCharges()
        {
            const decimal dailyBaseCharges = 350;
            int daysInput;
            decimal totalBaseCharges = 0;
            if (NumberOfDays == null)
            {
                ErrorForDaysInput = "Please Enter a Valid Input Here";
            }
            else if (!(int.TryParse(NumberOfDays, out daysInput) && daysInput >= 0))
            {
                ErrorForDaysInput = "Invalid Input";
            }
            else
            {
                totalBaseCharges = daysInput * dailyBaseCharges;
            }
            return totalBaseCharges;
        }
        private decimal CalcMiscCharges()
        {
            decimal medicationChargesInput, surgicalChargesInput, labFeesInput, physicalRehabitationChargesInput;
            decimal totalMiscCharges = 0;
            if (MedicationCharges == null)
            {
                ErrorForMedicationCharges = "Please Enter a Valid Input Here";
            }
            else if (!(decimal.TryParse(MedicationCharges, out medicationChargesInput) && medicationChargesInput >= 0))
            {
                ErrorForMedicationCharges = "Invalid Input";
            }
            else
            {
                totalMiscCharges += medicationChargesInput;
            }

            if (SurgicalCharges == null)
            {
                ErrorForSurgicalCharges = "Please Enter a Valid Input Here";
            }
            else if (!(decimal.TryParse(SurgicalCharges, out surgicalChargesInput) && surgicalChargesInput >= 0))
            {
                ErrorForSurgicalCharges = "Invalid Input";
            }
            else
            {
                totalMiscCharges += surgicalChargesInput;
            }

            if (LabFees == null)
            {
                ErrorForLabFees = "Please Enter a Valid Input Here";
            }
            else if (!(decimal.TryParse(LabFees, out labFeesInput) && labFeesInput >= 0))
            {
                ErrorForLabFees = "Invalid Input";
            }
            else
            {
                totalMiscCharges += labFeesInput;
            }

            if (PhysicalRehabitationCharges == null)
            {
                ErrorForPhysicalRehabitationCharges = "Please Enter a Valid Input Here";
            }
            else if (!(decimal.TryParse(PhysicalRehabitationCharges, out physicalRehabitationChargesInput) && physicalRehabitationChargesInput >= 0))
            {
                ErrorForPhysicalRehabitationCharges = "Invalid Input";
            }
            else
            {
                totalMiscCharges += physicalRehabitationChargesInput;
            }
            return totalMiscCharges;
        }
        private decimal CalcTotalCharges()
        {
            return CalcStayCharges() + CalcMiscCharges();
        }
        public void TotalCharges()
        {
            ClearAllErrorMsz();
            decimal x = CalcTotalCharges();
            if (ErrorForDaysInput == "" && ErrorForMedicationCharges == "" && ErrorForSurgicalCharges == "" && ErrorForLabFees == "" && ErrorForPhysicalRehabitationCharges == "")
            {
                Output = $"Total Charges are: ${x}";
            }
            else
            {
                Output = "Please Solve the Errors First";
            }
        }
        public void MiscCharges()
        {
            ClearAllErrorMsz();
            decimal x = CalcMiscCharges();
            if (ErrorForMedicationCharges == "" && ErrorForSurgicalCharges == "" && ErrorForLabFees == "" && ErrorForPhysicalRehabitationCharges == "")
            {
                Output = $"Total Misc Charges are: ${x}";
            }
            else
            {
                Output = "Please Solve the Errors First";
            }
        }
        public void StayCharges()
        {
            ClearAllErrorMsz();
            decimal x = CalcStayCharges();
            if (ErrorForDaysInput == "")
            {
                Output = $"Total Stay Charges are: ${x}";
            }
            else
            {
                Output = "Please Solve the Errors First";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnChange([CallerMemberName]string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void ClearAllErrorMsz()
        {
            ErrorForDaysInput = "";
            ErrorForMedicationCharges = "";
            ErrorForSurgicalCharges = "";
            ErrorForLabFees = "";
            ErrorForPhysicalRehabitationCharges = "";
            Output = "";
        }
    }
}
