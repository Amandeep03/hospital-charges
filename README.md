# Introduction
An application that calculates the total cost of a hospital stay. The daily base charge is $350. The hospital also charges for medication, surgical fees, lab fees, and physical rehabilitation by accepting the following input:

* The number or days spent in the hospital
* The amount of medication charges
* The amount of surgical charges
* The amount of lab fees
* The amount of physical rehabilitation charges
---


# Requirements 
* Download the source code of the project.
* Open Hospital Charges.sln file in Visual Studio
* Build the solution
* Run the project 

# Copyright © Amandeep Singh - 2020
